using System;
using System.Collections.Generic;

namespace SingleResponsibilityPrincipleStory.Models
{
    public class Person
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public DateTime Birthday { get; set; }

        public List<Address> Addresses { get; set; }

        public List<Phone> Phones { get; set; }
       
        public List<Contact> Contacts { get; set; }

        public override bool Equals(object obj)
        {
            var item = obj as Person;

            if (item == null)
            {
                return false;
            }

            return FirstName.Equals(item.FirstName) && 
                   LastName.Equals(item.LastName) && 
                   Birthday.Equals(item.Birthday);
        }

        public override int GetHashCode()
        {
            return FirstName.GetHashCode() ^ LastName.GetHashCode() ^ Birthday.GetHashCode();
        }
    }
}
