namespace SingleResponsibilityPrincipleStory.Models
{
    public class Address
    {
        public enum AddressType
        {
            HOME,
            WORK
        }

        public AddressType Type { get; set; }

        public string Address1 { get; set; }

        public string Address2 { get; set; }

        public string Zip { get; set; }

        public string Country { get; set; }
    }
}
