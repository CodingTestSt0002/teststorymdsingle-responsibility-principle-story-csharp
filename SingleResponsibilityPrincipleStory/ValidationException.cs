using System;

namespace SingleResponsibilityPrincipleStory
{
    public class ValidationException: Exception
    {
        public ValidationException(string message) : base(message)
        {
        }
    }
}
