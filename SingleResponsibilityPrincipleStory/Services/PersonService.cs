﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Logging;
using SingleResponsibilityPrincipleStory.Models;

namespace SingleResponsibilityPrincipleStory.Services
{
    public class PersonService
    {
        private static HashSet<Person> persons = new HashSet<Person>();
               
        private readonly ValidationService validationService;
        private readonly EmailService emailService;

        private readonly ILogger<PersonService> logger;

        public PersonService( ValidationService validationService,
            EmailService emailService,
            ILogger<PersonService> logger)
        {                        
            this.validationService = validationService;
            this.emailService = emailService;
            this.logger = logger;
        }
        public Person FindPersonByEmail(string email)
        {
            validationService.ValidateEmail(email);
            return persons.FirstOrDefault(p => p.Contacts.Any(c =>
                c.Type == Contact.ContactType.EMAIL &&
                email.Equals(c.ContactData, StringComparison.InvariantCultureIgnoreCase)));
        }

        public Person StorePerson(Person person)
        {
            validationService.ValidatePerson(person);
            persons.Add(person);
            logger.LogInformation($"New Person added to repository: {person.FirstName} {person.LastName}");
            emailService.GreetPerson(person);
            return person;
        }
    }
}
